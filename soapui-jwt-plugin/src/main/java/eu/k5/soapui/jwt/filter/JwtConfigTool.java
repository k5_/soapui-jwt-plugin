package eu.k5.soapui.jwt.filter;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.SwingUtilities;

import com.eviware.soapui.impl.WorkspaceImpl;
import com.eviware.soapui.plugins.ActionConfiguration;
import com.eviware.soapui.plugins.ToolbarPosition;
import com.eviware.soapui.support.action.support.AbstractSoapUIAction;

import eu.k5.soapui.jwt.filter.dialog.ConfigDialogModel;

@ActionConfiguration(actionGroup = "WorkspaceImplActions", //
		toolbarPosition = ToolbarPosition.NONE, //
		toolbarIcon = "/favicon.png", //
		description = "Jwt Token Configuration Tool")
public class JwtConfigTool extends AbstractSoapUIAction<WorkspaceImpl> {
	private final ConfigDialogModel dialogModel;

	public JwtConfigTool() {
		super("Jwt Config", "Configures JWT Provider");

		Path path = Paths.get(System.getProperty("user.home")).resolve(".soapuios").resolve("jwtConfig.xml");
		dialogModel = new ConfigDialogModel(path);
	}

	@Override
	public void perform(WorkspaceImpl workspace, Object o) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				dialogModel.display();
			}
		});
	}

}
