package eu.k5.soapui.jwt.filter.provider;

import com.jayway.jsonpath.JsonPath;
import com.steadystate.css.parser.ExceptionResource;

import eu.k5.soapui.jwt.filter.JwtConfiguration.EnvironmentConfiguration;
import eu.k5.soapui.jwt.filter.provider.TokenProvider.Token;
import net.minidev.json.JSONArray;

public class TokenExtractor {

	private JsonPath tokenExtract;

	private JsonPath expireExtract;

	public TokenExtractor(EnvironmentConfiguration config) {
		this(config.getTokenExtract(), config.getExpireExtract());
	}

	public TokenExtractor(String tokenExpression, String expireExpression) {
		tokenExtract = JsonPath.compile(tokenExpression);
		expireExtract = JsonPath.compile(expireExpression);
	}

	String token(String json) {
		return tokenExtract.read(json).toString();
	}

	int expire(String json) {
		return Integer.valueOf(expireExtract.read(json).toString());
	}

	public Token extract(String json) {

		String token = token(json);
		int expires = expire(json);

		return new Token(token, expires);
	}
}
