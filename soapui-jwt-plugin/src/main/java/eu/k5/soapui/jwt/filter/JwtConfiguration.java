package eu.k5.soapui.jwt.filter;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class JwtConfiguration {

	@XmlElement(name = "active")
	private Boolean active;

	@XmlElement(name = "environments")
	private List<EnvironmentConfiguration> environments = new ArrayList<>();

	@XmlAccessorType(XmlAccessType.NONE)
	public static class EnvironmentConfiguration {

		@XmlAttribute(name = "name")
		private String name;

		@XmlAttribute(name = "default")
		private Boolean isDefault;

		@XmlAttribute(name = "active")
		private Boolean active;

		@XmlElement(name = "keystore")
		private String keystore;

		@XmlElement(name = "password")
		private String keystorePassword;

		@XmlElement(name = "tokenProviderEndpoint")
		private String tokenProviderEndpoint;

		@XmlElement(name = "tokenProviderRequest")
		private String tokenProviderRequest;

		@XmlElement(name = "tokenExtract")
		private String tokenExtract;

		@XmlElement(name = "expireExtract")
		private String expireExtract;

		@XmlElement(name = "proxy")
		private String proxy;

		@XmlElement(name = "header")
		private List<Header> headers = new ArrayList<>();

		public Boolean getActive() {
			return active;
		}

		public void setActive(Boolean active) {
			this.active = active;
		}

		public List<Header> getHeaders() {
			return headers;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public Boolean getIsDefault() {
			return isDefault;
		}

		public void setIsDefault(Boolean isDefault) {
			this.isDefault = isDefault;
		}

		public String getProxy() {
			return proxy;
		}

		public void setProxy(String proxy) {
			this.proxy = proxy;
		}

		public String getKeystore() {
			return keystore;
		}

		public void setKeystore(String keystore) {
			this.keystore = keystore;
		}

		public String getKeystorePassword() {
			return keystorePassword;
		}

		public void setKeystorePassword(String keystorePassword) {
			this.keystorePassword = keystorePassword;
		}

		public String getTokenProviderEndpoint() {
			return tokenProviderEndpoint;
		}

		public void setTokenProviderEndpoint(String tokenProviderEndpoint) {
			this.tokenProviderEndpoint = tokenProviderEndpoint;
		}

		public String getTokenProviderRequest() {
			return tokenProviderRequest;
		}

		public void setTokenProviderRequest(String tokenProviderRequest) {
			this.tokenProviderRequest = tokenProviderRequest;
		}

		public String getTokenExtract() {
			return tokenExtract;
		}

		public void setTokenExtract(String tokenExtract) {
			this.tokenExtract = tokenExtract;
		}

		public String getExpireExtract() {
			return expireExtract;
		}

		public void setExpireExtract(String expireExtract) {
			this.expireExtract = expireExtract;
		}
	}

	@XmlAccessorType(XmlAccessType.NONE)
	public static class Header {
		@XmlAttribute(name = "key")
		private String key;
		@XmlAttribute(name = "value")
		private String value;

		public Header() {
		}

		public Header(String key, String value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	public List<EnvironmentConfiguration> getEnvironments() {
		return environments;
	}

	public EnvironmentConfiguration getDefaultEnvironment() {
		for (EnvironmentConfiguration config : getEnvironments()) {
			if (Boolean.TRUE.equals(config.isDefault)) {
				return config;
			}
		}
		return null;
	}

	public EnvironmentConfiguration getEnvironmentByName(String name) {
		for (EnvironmentConfiguration config : getEnvironments()) {
			if (name.equals(config.name)) {
				return config;
			}
		}
		return null;

	}

}
