package eu.k5.soapui.jwt.filter.provider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.jayway.jsonpath.JsonPath;

import eu.k5.soapui.jwt.filter.JwtConfiguration;
import eu.k5.soapui.jwt.filter.JwtConfiguration.EnvironmentConfiguration;
import eu.k5.soapui.jwt.filter.provider.JwtClient.Builder;

public class TokenProvider {

	public static final String DEFAULT = "default";

	private JwtConfiguration configuration = new JwtConfiguration();

	private Map<String, Token> tokens = new HashMap<>();

	private TokenProvider() {

	}

	public synchronized boolean isActive() {
		return true;
	}

	public synchronized String getToken(String name) {
		Token token = tokens.get(name);
		if (token == null) {
			token = create(name);
			tokens.put(name, token);
		}
		return null;
	}

	public Token testConfiguration(EnvironmentConfiguration config) {

		TokenExtractor extractor = new TokenExtractor(config);
		return createForConfig(config, extractor);
	}

	private Token create(String name) {
		EnvironmentConfiguration config;
		if (DEFAULT.equals(name)) {
			config = configuration.getDefaultEnvironment();
		} else {
			config = configuration.getEnvironmentByName(name);
		}
		if (config == null) {
			return null;
		}
		TokenExtractor extractor = new TokenExtractor(config);
		return createForConfig(config, extractor);

	}

	private Token createForConfig(EnvironmentConfiguration config, TokenExtractor extractor) {

		Builder builder = JwtClient.builder();
		//.withKeystore(readKeyStore(config.getKeystore()));
		builder.withKeystorePassword(config.getKeystorePassword());
		builder.withEndpoint(config.getTokenProviderEndpoint()).withRequestBody(config.getTokenProviderRequest());
		JwtClient client = builder.build();

		String response = client.invoke();

		return extractor.extract(response);
	}

	private byte[] readKeyStore(String keystore) {
		if (StringUtils.isEmpty(keystore)) {
			return null;
		}
		Path path = Paths.get(keystore);
		if (!Files.exists(path)) {
			throw new TokenCreateException("Keystore in Path: '" + keystore + "' does not exist");
		}
		try {
			return Files.readAllBytes(path);
		} catch (IOException e) {
			throw new TokenCreateException("Unable to read keystore: '" + keystore + "'", e);
		}
	}

	private static TokenProvider instance;

	public synchronized static TokenProvider getInstance() {
		if (instance == null) {
			instance = new TokenProvider();
		}
		return instance;
	}

	public static class Token {
		private final String token;
		private final long expires;

		public Token(String token, long expires) {
			this.token = token;
			this.expires = expires;
		}

		public String getToken() {
			return token;
		}

		public long getExpires() {
			return expires;
		}

		public boolean isExpired() {
			// Does not expire in next minute
			return System.currentTimeMillis() + 60_000 > expires;
		}

	}

	public synchronized void reset() {

	}

}
