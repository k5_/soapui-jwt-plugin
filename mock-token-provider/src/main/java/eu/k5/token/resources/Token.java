package eu.k5.token.resources;

public class Token {

	private String token;

	private int expires;

	public Token() {
	}

	public Token(String token, int expires) {
		this.token = token;
		this.expires = expires;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getExpires() {
		return expires;
	}

	public void setExpires(int expires) {
		this.expires = expires;
	}

}
