package eu.k5.soapui.jwt.filter.provider;

public class TokenCreateException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public TokenCreateException(String message, Throwable cause) {
		super(message, cause);
	}

	public TokenCreateException(String message) {
		super(message);
	}

}
