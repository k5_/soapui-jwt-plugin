package eu.k5.soapui.jwt.filter.dialog;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import eu.k5.soapui.jwt.filter.JwtConfiguration;
import eu.k5.soapui.jwt.filter.JwtConfiguration.EnvironmentConfiguration;

public class ConfigDialogView {

	private JFrame mainFrame;
	private GridBagLayout inputLayout;

	private TextField nameField;
	private TextField jwtProviderField;

	private BorderLayout mainLayout;
	private Panel inputPanel;

	private Panel buttonPanel = new Panel();
	private final ConfigDialogController controller;
	private TextField jwtRequestField;
	private TextField jwtKeyStoreField;
	private TextField proxy;
	private TextField jwtTokenExtractField;
	private TextField keyStorePasswordField;
	private TextField jwtExpireExtractField;
	private ConfigDialogModel model;

	public ConfigDialogView(ConfigDialogModel model) {
		this.model = model;
		controller = new ConfigDialogController(model, this);
	}

	void init() {
		mainFrame = new JFrame("Jwt Configuration Dialog");

		mainLayout = new BorderLayout(2, 2);

		inputLayout = new GridBagLayout();
		inputPanel = new Panel(inputLayout);

		mainFrame.setLayout(mainLayout);

		mainFrame.add(buttonPanel);
		mainLayout.addLayoutComponent(buttonPanel, BorderLayout.SOUTH);

		mainFrame.add(inputPanel);
		mainLayout.addLayoutComponent(inputPanel, BorderLayout.CENTER);

		nameField = addLabelWithInput("Name");
		jwtProviderField = addLabelWithInput("Jwt Provider");
		jwtRequestField = addLabelWithInput("Jwt Request");
		jwtTokenExtractField = addLabelWithInput("Extract Token");
		jwtExpireExtractField = addLabelWithInput("Expire Extract");

		addDelimiter();
		jwtKeyStoreField = addLabelWithInput("KeyStore");
		keyStorePasswordField = addLabelWithInput("Password");
		addDelimiter();
		proxy = addLabelWithInput("Proxy");

		addButtons();

		mainFrame.pack();
		mainFrame.setSize(300, 300);
	}
	
	private void addSelect(){
	}

	private void addDelimiter() {
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.weightx = 1.0;
		constraints.gridwidth = GridBagConstraints.REMAINDER; // next-to-last in

		Label empty = new Label();

		inputLayout.addLayoutComponent(empty, constraints);
		inputPanel.add(empty);
	}

	private void addButtons() {
		new BoxLayout(buttonPanel, BoxLayout.X_AXIS);
		Label label = new Label("");
		buttonPanel.add(label);

		Button test = new Button("Test");
		test.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.test();
			}
		});
		buttonPanel.add(test);

		Button cancel = new Button("Cancel");
		cancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.cancel();
			}
		});
		buttonPanel.add(cancel);

		Button save = new Button("Save");
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.save();
			}
		});
		buttonPanel.add(save);
	}

	private TextField addLabelWithInput(String labelText) {
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 0.0;

		constraints.gridwidth = GridBagConstraints.RELATIVE;
		Label label = new Label(labelText);
		inputLayout.setConstraints(label, constraints);
		inputPanel.add(label);

		constraints.weightx = 1.0;
		constraints.gridwidth = GridBagConstraints.REMAINDER; // next-to-last in
																// row
		TextField textField = new TextField();
		inputLayout.setConstraints(textField, constraints);
		inputPanel.add(textField);
		return textField;
	}

	public JwtConfiguration.EnvironmentConfiguration getCurrent() {
		EnvironmentConfiguration config = new EnvironmentConfiguration();
		config.setName(this.nameField.getText());
		config.setIsDefault(false);
		config.setExpireExtract(this.jwtExpireExtractField.getText());
		config.setTokenExtract(this.jwtTokenExtractField.getText());
		config.setTokenProviderRequest(this.jwtRequestField.getText());
		config.setTokenProviderEndpoint(this.jwtProviderField.getText());
		config.setKeystore(this.jwtKeyStoreField.getText());
		config.setKeystorePassword(this.keyStorePasswordField.getText());
		config.setProxy(this.proxy.getText());
		return config;
	}

	public void display() {
		controller.reset();
		mainFrame.setVisible(true);
	}

	public void setCurrent(EnvironmentConfiguration config) {
		this.nameField.setText(config.getName());
		this.jwtExpireExtractField.setText(config.getExpireExtract());
		this.jwtTokenExtractField.setText(config.getTokenExtract());
		this.jwtProviderField.setText(config.getTokenProviderEndpoint());
		this.jwtRequestField.setText(config.getTokenProviderRequest());
		this.jwtKeyStoreField.setText(config.getKeystore());
		this.keyStorePasswordField.setText(config.getKeystorePassword());
		this.proxy.setText(config.getProxy());
	}
}
