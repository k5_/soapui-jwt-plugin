package eu.k5.token;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.server.Dispatcher;

import com.roskart.dropwizard.jaxws.EndpointBuilder;
import com.roskart.dropwizard.jaxws.JAXWSBundle;

import eu.k5.token.resources.TokenResource;
import eu.k5.token.server.StockQuoteImpl;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TokenApplication extends Application<TokenConfiguration> {
	private JAXWSBundle<TokenConfiguration> jaxWsBundle = new JAXWSBundle<TokenConfiguration>();

	public void initialize(Bootstrap<TokenConfiguration> bootstrap) {
		bootstrap.addBundle(jaxWsBundle);
	}

	@Override
	public void run(TokenConfiguration config, Environment env) throws Exception {

		env.servlets().addFilter("filter", new CustomFilter())
				.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

		jaxWsBundle.publishEndpoint(new EndpointBuilder("/hello", new StockQuoteImpl()));
        
		
		env.jersey().setUrlPattern("/rest/*");
		env.jersey().register(new TokenResource());
	}

	public static void main(String[] args) throws Exception {
		new TokenApplication().run(args);
	}

}
