package eu.k5.soapui.jwt.filter.provider;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.css.sac.InputSource;

import eu.k5.soapui.jwt.filter.JwtConfiguration.EnvironmentConfiguration;

public class JwtClient {

	private final String endpoint;
	private final String requestBody;
	private Map<String, String> headers = new HashMap<>();

	private final HttpClient httpClient;

	private JwtClient(HttpClient httpClient, Builder builder) {
		this.httpClient = httpClient;
		this.endpoint = builder.endpoint;
		this.requestBody = builder.requestBody;
	}

	public String invoke() {

		HttpPost post = new HttpPost();

		for (Entry<String, String> entry : headers.entrySet()) {
			post.addHeader(entry.getKey(), entry.getValue());
		}

		try {
			post.setURI(new URI(endpoint));

			HttpResponse response = httpClient.execute(post);

			String content = readContent(response.getEntity().getContent());

			return content;
		} catch (IOException | URISyntaxException e) {
			throw new RuntimeException(e);
		} finally {
		}
	}

	private String readContent(InputStream is) throws IOException {
		byte[] bytes = IOUtils.toByteArray(is);
		return new String(bytes, StandardCharsets.UTF_8);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private String endpoint;
		private String requestBody;
		private String proxyHost;
		private int proxyPort = -1;
		private Map<String, String> headers = new HashMap<>();

		private byte[] keystore;
		private String keystorePassword;

		public Builder withEndpoint(String endpoint) {
			this.endpoint = endpoint;
			return this;
		}

		public Builder withRequestBody(String requestBody) {
			this.requestBody = requestBody;
			return this;
		}

		public Builder withKeystore(byte[] keystore) {
			this.keystore = keystore;
			return this;
		}

		public Builder withKeystorePassword(String keystorePassword) {
			this.keystorePassword = keystorePassword;
			return this;
		}

		public Builder addHeader(String key, String value) {
			this.headers.put(key, value);
			return this;
		}

		public Builder withProxy(String proxy) {
			if (proxy != null && proxy.isEmpty()){
				return this;
			}
			int delimiter = proxy.indexOf(':');
			if (delimiter < 0) {
				throw new RuntimeException("No port separator found");
			} else {
				withProxyHost(proxy.substring(0, delimiter));
				withProxyPort(Integer.parseInt(proxy.substring(delimiter+1)));
			}
			return this;
		}

		public Builder withProxyHost(String proxyHost) {
			this.proxyHost = proxyHost;
			return this;
		}

		public Builder withProxyPort(int proxyPort) {
			this.proxyPort = proxyPort;
			return this;
		}

		public JwtClient build() {
			HttpClient httpClient;
			if (keystore == null) {
				httpClient = createHttpClient();
			} else {
				httpClient = createCertHttpClient();
			}

			return new JwtClient(httpClient, this);
		}

		private HttpClient createCertHttpClient() {

			return new DefaultHttpClient();
		}

		private void getKeystore() {
			try {
				// TrustManagerFactory tmf =
				// TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore ks = KeyStore.getInstance("JKS");
				ks.load(new ByteArrayInputStream(keystore), keystorePassword.toCharArray());

			} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// tmf.init(ks);
		}

		private HttpClient createHttpClient() {
			return new DefaultHttpClient();
		}

	}
}
