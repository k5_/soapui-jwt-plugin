package eu.k5.token.server;

import com.example.stockquote.TradePrice;
import com.example.stockquote.TradePriceRequest;
import com.example.stockquote_wsdl.StockQuotePortType;

public class StockQuoteImpl implements StockQuotePortType {
	private static final com.example.stockquote.ObjectFactory OBJECT_FACTORY = new com.example.stockquote.ObjectFactory();

	public TradePrice getLastTradePrice(TradePriceRequest body) {
		
		TradePrice tradePrice = OBJECT_FACTORY.createTradePrice();
		tradePrice.setPrice(1.5f);
		return tradePrice;
	}

}
