package eu.k5.token;

import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;

import javax.security.cert.X509Certificate;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class CustomFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {

			X509Certificate[] certificateChain = (X509Certificate[]) request
					.getAttribute("javax.servlet.request.X509Certificate");
			Principal pr = ((HttpServletRequest) request).getUserPrincipal();

			Enumeration<String> headers = ((HttpServletRequest) request).getHeaderNames();
			while (headers.hasMoreElements()) {
				String header = headers.nextElement();
				System.out.println(((HttpServletRequest) request).getHeader(header));
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
