package eu.k5.soapui.jwt.filter;

import com.eviware.soapui.plugins.PluginAdapter;
import com.eviware.soapui.plugins.PluginConfiguration;

@PluginConfiguration(groupId = "com.mycompany", name = "Jwt Request Filter", version = "1.0.0", autoDetect = true, description = "A plugin for finding project items", infoUrl = "http://olensmar.blogspot.com/2014/07/getting-started-with-new-soapui-plugin.html")
public class PluginConfig extends PluginAdapter {
}