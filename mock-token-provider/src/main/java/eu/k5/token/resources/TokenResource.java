package eu.k5.token.resources;

import java.util.function.Predicate;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("token")
public class TokenResource {

	@GET
	public String alive() {
		return "yes";
	}

	@POST
	@Produces({"application/json", "application/x-www-form-urlencoded"})
	public Token create() {
		
		String string = "sad";
		Predicate<? super String> x = "sad"::contains;
		
		return new Token("abc", 100);
	}
}
