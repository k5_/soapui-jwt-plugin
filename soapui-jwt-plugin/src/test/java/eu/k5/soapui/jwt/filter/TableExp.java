package eu.k5.soapui.jwt.filter;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

public class TableExp {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				createAndDisplay();
			}
		});
	}

	private static void createAndDisplay() {
		JFrame mainFrame = new JFrame("Jwt Configuration Dialog");

		JTable table = new JTable(new MyModel());
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);

		mainFrame.setContentPane(scrollPane);

		mainFrame.setSize(400, 400);
		mainFrame.setVisible(true);
	}

	static class MyModel extends AbstractTableModel {
		private Object[][] data = { { "Kathy", "Smith", "Snowboarding", new Integer(5), new Boolean(false) },
				{ "John", "Doe", "Rowing", new Integer(3), new Boolean(true) },
				{ "Sue", "Black", "Knitting", new Integer(2), new Boolean(false) },
				{ "Jane", "White", "Speed reading", new Integer(20), new Boolean(true) },
				{ "Joe", "Brown", "Pool", new Integer(10), new Boolean(false) } };

		private Object[] columns = { "Name", "Firstname", "Hobby", "years", "x" };

		
		public String getColumnName(int col) {
			return columns[col].toString();
		}

		public int getRowCount() {
			return data.length;
		}

		public int getColumnCount() {
			return columns.length;
		}

		public Object getValueAt(int row, int col) {
			return data[row][col];
		}

		public boolean isCellEditable(int row, int col) {
			return true;
		}

		public void setValueAt(Object value, int row, int col) {
			data[row][col] = value;
			fireTableCellUpdated(row, col);
		}
	}
}
