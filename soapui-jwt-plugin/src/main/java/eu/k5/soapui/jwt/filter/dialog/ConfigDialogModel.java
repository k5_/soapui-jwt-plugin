package eu.k5.soapui.jwt.filter.dialog;

import java.nio.file.Files;
import java.nio.file.Path;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import eu.k5.soapui.jwt.filter.JwtConfiguration;
import eu.k5.soapui.jwt.filter.provider.TokenProvider;

public class ConfigDialogModel {
	private final Path path;

	private final JAXBContext context;
	private ConfigDialogView view;

	public ConfigDialogModel(Path path) {
		this.path = path;
		try {
			this.context = JAXBContext.newInstance(JwtConfiguration.class);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	public Path getPath() {
		return path;
	}

	public JwtConfiguration read() {
		if (Files.exists(getPath())) {
			Unmarshaller unmarshaller;
			try {
				unmarshaller = getContext().createUnmarshaller();
				Object object = unmarshaller.unmarshal(getPath().toFile());
				return (JwtConfiguration) object;
			} catch (JAXBException e) {
				throw new RuntimeException(e);
			}
		}
		return new JwtConfiguration();

	}

	public void write(JwtConfiguration mainConfig) {
		try {
			Marshaller marshaller = getContext().createMarshaller();
			marshaller.marshal(mainConfig, getPath().toFile());
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		TokenProvider.getInstance().reset();
	}

	public synchronized void display() {
		if (view == null) {
			view = new ConfigDialogView(this);
			view.init();
		}
		view.display();
	}

	public JAXBContext getContext() {
		return context;
	}

}
