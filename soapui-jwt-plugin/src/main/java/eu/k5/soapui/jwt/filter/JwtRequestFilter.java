package eu.k5.soapui.jwt.filter;

import java.util.List;

import com.eviware.soapui.impl.support.AbstractHttpRequestInterface;
import com.eviware.soapui.impl.wsdl.WsdlRequest;
import com.eviware.soapui.impl.wsdl.submit.filters.AbstractRequestFilter;
import com.eviware.soapui.impl.wsdl.submit.transports.http.BaseHttpRequestTransport;
import com.eviware.soapui.impl.wsdl.submit.transports.http.HttpResponse;
import com.eviware.soapui.model.iface.SubmitContext;
import com.eviware.soapui.plugins.auto.PluginRequestFilter;
import com.eviware.soapui.support.types.StringToStringsMap;

import eu.k5.soapui.jwt.filter.provider.TokenProvider;

@PluginRequestFilter(protocol = "http")
public class JwtRequestFilter extends AbstractRequestFilter {

	@Override
	public void filterWsdlRequest(SubmitContext context, WsdlRequest request) {

		if (!TokenProvider.getInstance().isActive()) {
			return;
		}

		StringToStringsMap headers = request.getRequestHeaders();
		List<String> authorization = headers.get("Authorization");

		headers.add("abc", "xyz"); 
		
		request.setRequestHeaders(headers);
		
		System.out.println("filtering wsdl request");
	}

	@Override
	public void afterAbstractHttpResponse(SubmitContext context, AbstractHttpRequestInterface<?> request) {
		HttpResponse response = (HttpResponse) context.getProperty(BaseHttpRequestTransport.RESPONSE);
		response.setProperty("Secret Message", "bu!");
	}
}