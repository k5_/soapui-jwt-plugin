package eu.k5.soapui.jwt.filter.dialog;

import eu.k5.soapui.jwt.filter.JwtConfiguration;
import eu.k5.soapui.jwt.filter.provider.TokenProvider;

public class ConfigDialogController {

	private final ConfigDialogView view;
	private final ConfigDialogModel model;

	public ConfigDialogController(ConfigDialogModel model, ConfigDialogView view) {
		this.model = model;
		this.view = view;
	}

	public void reset() {
		JwtConfiguration config = model.read();
		if (config.getEnvironments().size() == 1) {
			view.setCurrent(config.getEnvironments().get(0));
		}
	}

	public void cancel() {

	}

	public void save() {
		JwtConfiguration mainConfig = new JwtConfiguration();
		mainConfig.getEnvironments().add(view.getCurrent());
		
		model.write(mainConfig);
	}

	public void test() {
		TokenProvider.getInstance().testConfiguration(view.getCurrent());
	}

}
